package com.assignment.multithread.file;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileReaderTest {

	@Value("classpath:../classes/file1.txt")
	private Resource inputFile1;

	@Value("classpath:../classes/file2.txt")
	private Resource inputFile2;

	Logger logger = LoggerFactory.getLogger(FileReaderTest.class);

	@Value("classpath:../classes/errorFile.txt")
	private Resource errorFile;

	@Value("classpath:../classes/bigFile.txt")
	private Resource bigFile;

	@Test
	public void A_check_if_can_read_file1() throws IOException {
		Assert.assertTrue(inputFile1.getFile().exists());
	}

	@Test
	public void B_check_if_can_read_file2() throws IOException {
		Assert.assertTrue(inputFile2.getFile().exists());
	}

	@Test
	public void C_read_small_file() throws Exception {
		readFile(inputFile1.getFile());
	}

	@Test(expected = Exception.class)
	public void D_read_small_file_with_errors() throws Exception {
		readFile(errorFile.getFile());
	}

	@Test()
	public void E_read_big_file() throws Exception {
		readFile(bigFile.getFile());
	}

	@Test()
	public void F_check_threads() throws Exception {

		Executor executor = Executors.newFixedThreadPool(2);

		CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> "Hello", executor)
				.thenCombine(CompletableFuture.supplyAsync(() -> " World", executor), (s1, s2) -> s1 + s2);

		assertEquals("Hello World", completableFuture.get());

	}

	@Test()
	public void G_check_threads_read_error_files() throws Exception  {

		Executor executor = Executors.newFixedThreadPool(2);

	
		CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
			try {
				readFile(errorFile.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			return "Hello";

		}, executor).exceptionally(ex -> {			
			return "";
		}).thenCombine(CompletableFuture.supplyAsync(() -> {
			throw new RuntimeException();
//			return " World";
		}, executor), (s1, s2) -> s1 + s2)
		.exceptionally(ex -> {
			
			return "";
		});

		assertNotEquals("Hello World", completableFuture.get());

	}
	
	@Test()
	public void H_check_threads_count_words_from_file1_and_file2() throws Exception  {

		Executor executor = Executors.newFixedThreadPool(2);

		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap1 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(inputFile1.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
		}, executor);
		
	
		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap2 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(inputFile2.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			

		}, executor);
		
		
		CompletableFuture<LinkedHashMap<String, String> > combinedMap=wordOccurrencesMap1.thenCombineAsync(wordOccurrencesMap2, (m1,m2)-> {
						
			LinkedHashMap<String, Integer> mx = Stream.of(m1, m2)
			        .map(Map::entrySet)          
			        .flatMap(Collection::stream) 
			                                     
			        .collect(
			            Collectors.toMap(       
			                Map.Entry::getKey,   
			                Map.Entry::getValue, 
			                Integer::sum        
			            )
			        )
			        .entrySet().stream().
			        sorted( Map.Entry.<String, Integer>comparingByValue().reversed()).
				    collect(Collectors.toMap(Entry::getKey, Entry::getValue,
				                             (e1, e2) -> e1, LinkedHashMap::new)); 
			    ;
			    
			
		    LinkedHashMap<String, String> finalResult=new LinkedHashMap<String, String>();
			    
			mx.forEach((k, v) -> {
				
				m1.putIfAbsent(k, 0);
				m2.putIfAbsent(k, 0);
				
				finalResult.put(k+v,m1.get(k)+"+"+m2.get(k));
				
			});
			
		    return finalResult;
		});

		
		combinedMap.get();
		
		assertNotEquals(0,  combinedMap.get().values().size());

		logger.info(combinedMap.get().toString());
		
	}
	@Test()
	public void I_check_threads_count_words_from_file1_and_bigFile() throws Exception  {

		Executor executor = Executors.newFixedThreadPool(2);

		
		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap1 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(bigFile.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			

		}, executor);
		
	
		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap2 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(inputFile2.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			

		}, executor);
		
		
		CompletableFuture<LinkedHashMap<String, String> > combinedMap=wordOccurrencesMap1.thenCombineAsync(wordOccurrencesMap2, (m1,m2)-> {
						
			LinkedHashMap<String, Integer> mx = Stream.of(m1, m2)
			        .map(Map::entrySet)          
			        .flatMap(Collection::stream) 
			                                     
			        .collect(
			            Collectors.toMap(       
			                Map.Entry::getKey,   
			                Map.Entry::getValue, 
			                Integer::sum        
			            )
			        )
			        .entrySet().stream().
			        sorted( Map.Entry.<String, Integer>comparingByValue().reversed()).
				    collect(Collectors.toMap(Entry::getKey, Entry::getValue,
				                             (e1, e2) -> e1, LinkedHashMap::new)); 
			    ;
			    
			
		    LinkedHashMap<String, String> finalResult=new LinkedHashMap<String, String>();
			    
			mx.forEach((k, v) -> {
				
				m1.putIfAbsent(k, 0);
				m2.putIfAbsent(k, 0);
				
				finalResult.put(k+v,m1.get(k)+"+"+m2.get(k));
				
			});
			
		    return finalResult;
		});

		
		combinedMap.get();
		
		assertNotEquals(0,  combinedMap.get().values().size());

		logger.info(combinedMap.get().toString());
		
	}
	@Test
	public void J_check_threads_count_words_from_file1_and_bigFile_print_multi_row() throws Exception  {
		Executor executor = Executors.newFixedThreadPool(2);

		
		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap1 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(bigFile.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			

		}, executor);
		
	
		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap2 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(inputFile2.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			

		}, executor);
		
		
		CompletableFuture<String> combinedMap=wordOccurrencesMap1.thenCombineAsync(wordOccurrencesMap2, (m1,m2)-> {
						
			LinkedHashMap<String, Integer> mx = Stream.of(m1, m2)
			        .map(Map::entrySet)          
			        .flatMap(Collection::stream) 
			                                     
			        .collect(
			            Collectors.toMap(       
			                Map.Entry::getKey,   
			                Map.Entry::getValue, 
			                Integer::sum        
			            )
			        )
			        .entrySet().stream().
			        sorted( Map.Entry.<String, Integer>comparingByValue().reversed()).
				    collect(Collectors.toMap(Entry::getKey, Entry::getValue,
				                             (e1, e2) -> e1, LinkedHashMap::new)); 
			    ;
			    
			
		    StringBuilder finalResult=new StringBuilder();
			    
			mx.forEach((k, v) -> {
				
				m1.putIfAbsent(k, 0);
				m2.putIfAbsent(k, 0);
				
				finalResult.append(k+v).append("=").append(m1.get(k)).append("+").append(+m2.get(k)).append("\n");
				
				
				
			});
			
		    return finalResult.toString();
		});

		String finalResult=combinedMap.get();
		
		assertNotEquals("",finalResult  );

		logger.info(finalResult);
	}
	
	@Test
	public void K_check_threads_count_words_from_file1_and_bigFile_print_multi_row_with_error_file() throws Exception  {
		Executor executor = Executors.newFixedThreadPool(2);

		
		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap1 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(bigFile.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			

		}, executor).exceptionally(ex -> {			
			return new ConcurrentSkipListMap<String, Integer>() ;
		});;
		
	
		CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap2 = CompletableFuture.supplyAsync(() -> {
			try {
				return countWordsFromFile(errorFile.getFile());
				
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				throw new RuntimeException(e);
			}
			
			

		}, executor).exceptionally(ex -> {			
			return new ConcurrentSkipListMap<String, Integer>() ;
		});
		
		
		CompletableFuture<String> combinedMap=wordOccurrencesMap1.thenCombineAsync(wordOccurrencesMap2, (m1,m2)-> {
						
			if (m1.isEmpty() || m2.isEmpty()) {
				return "";
			}
			
			LinkedHashMap<String, Integer> mx = Stream.of(m1, m2)
			        .map(Map::entrySet)          
			        .flatMap(Collection::stream) 
			                                     
			        .collect(
			            Collectors.toMap(       
			                Map.Entry::getKey,   
			                Map.Entry::getValue, 
			                Integer::sum        
			            )
			        )
			        .entrySet().stream().
			        sorted( Map.Entry.<String, Integer>comparingByValue().reversed()).
				    collect(Collectors.toMap(Entry::getKey, Entry::getValue,
				                             (e1, e2) -> e1, LinkedHashMap::new)); 
			    ;
			    
			
		    StringBuilder finalResult=new StringBuilder();
			    
			mx.forEach((k, v) -> {
				
				m1.putIfAbsent(k, 0);
				m2.putIfAbsent(k, 0);
				
				finalResult.append(k+v).append("=").append(m1.get(k)).append("+").append(+m2.get(k)).append("\n");
				
				
				
			});
			
		    return finalResult.toString();
		});

		String finalResult=combinedMap.get();
		
		assertEquals("",finalResult);

		logger.info(finalResult);
	}
	
	private ConcurrentMap<String, Integer> countWordsFromFile(File file) throws Exception{
		
		ConcurrentMap<String, Integer> map = new ConcurrentSkipListMap<String, Integer>();
		
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		try {
			while (it.hasNext()) {
				String line = it.nextLine();

				String lineCheck = line.replace(" ", "");

				boolean allLetters = lineCheck.chars().allMatch(Character::isLetter);

				if (!allLetters) {

					String message = "found unletterals characters in line :" + line;

					logger.error(message);
					throw new Exception(message);
				}
				
				List<String> splitted = Arrays.asList(StringUtils.split(line));

				splitted.forEach(word->{
					map.computeIfAbsent(word, k->1);
					map.computeIfPresent(word, (k,v)->++v);
				});
				
			}
		} finally {
			it.close();
		}
		return map;
	}
	
	
	
	private void readFile(File file) throws Exception {
		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		try {
			while (it.hasNext()) {
				String line = it.nextLine();

				String lineCheck = line.replace(" ", "");

				boolean allLetters = lineCheck.chars().allMatch(Character::isLetter);

				if (!allLetters) {

					String message = "found unletterals characters in line :" + line;

					logger.error(message);
					throw new Exception(message);
				}

			}
		} finally {
			it.close();
		}
	}
}
