package com.assignment.multithread.service;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("default")
public class WordCounterService {

	private static Logger logger = LoggerFactory.getLogger(WordCounterService.class);

	public ConcurrentMap<String, Integer> countWordsFromFile(File file) throws Exception {

		ConcurrentMap<String, Integer> map = new ConcurrentSkipListMap<String, Integer>();

		LineIterator it = FileUtils.lineIterator(file, "UTF-8");
		try {
			while (it.hasNext()) {
				String line = it.nextLine();

				String lineCheck = line.replace(" ", "");

				boolean allLetters = lineCheck.chars().allMatch(Character::isLetter);

				if (!allLetters) {

					String message = "found unletterals characters in line :" + line;

					logger.error(message);
					throw new Exception(message);
				}

				List<String> splitted = Arrays.asList(StringUtils.split(line));

				splitted.forEach(word -> {
					map.computeIfAbsent(word, k -> 1);
					map.computeIfPresent(word, (k, v) -> ++v);
				});

			}
		} finally {
			it.close();
		}
		return map;
	}

}
