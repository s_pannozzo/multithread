package com.assignment.multithread.component;


import java.io.File;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.assignment.multithread.service.WordCounterService;


@Component
@Profile("default")
public class MainStarter implements CommandLineRunner{

	private static Logger logger = LoggerFactory.getLogger(MainStarter.class);
	
	@Autowired
	private WordCounterService service;
	
	
	
	@Override
	public void run(String... args) throws Exception {
		if (args.length==2) {
			String pathFile1=args[0];
			
			String pathFile2=args[0];
			
			logger.debug("file 1 "+pathFile1);
			logger.debug("file 2 "+pathFile2);
			
			
			File file1=new File(pathFile1);
			File file2=new File(pathFile2);
			
			Executor executor = Executors.newFixedThreadPool(2);
			
			CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap1 = CompletableFuture.supplyAsync(() -> {
				try {
					return service.countWordsFromFile(file1);
					
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					throw new RuntimeException(e);
				}
				
				

			}, executor).exceptionally(ex -> {			
				return new ConcurrentSkipListMap<String, Integer>() ;
			});
			
			
			CompletableFuture<ConcurrentMap<String, Integer> > wordOccurrencesMap2 = CompletableFuture.supplyAsync(() -> {
				try {
					return service.countWordsFromFile(file2);
					
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
					throw new RuntimeException(e);
				}
				
				

			}, executor).exceptionally(ex -> {			
				return new ConcurrentSkipListMap<String, Integer>() ;
			});
			
			
			CompletableFuture<String> combinedMap=wordOccurrencesMap1.thenCombineAsync(wordOccurrencesMap2, (m1,m2)-> {
							
				if (m1.isEmpty() || m2.isEmpty()) {
					return "";
				}
				
				LinkedHashMap<String, Integer> mx = Stream.of(m1, m2)
				        .map(Map::entrySet)          
				        .flatMap(Collection::stream) 
				                                     
				        .collect(
				            Collectors.toMap(       
				                Map.Entry::getKey,   
				                Map.Entry::getValue, 
				                Integer::sum        
				            )
				        )
				        .entrySet().stream().
				        sorted( Map.Entry.<String, Integer>comparingByValue().reversed()).
					    collect(Collectors.toMap(Entry::getKey, Entry::getValue,
					                             (e1, e2) -> e1, LinkedHashMap::new)); 
				    ;
				    
				
			    StringBuilder finalResult=new StringBuilder();
				    
				mx.forEach((k, v) -> {
					
					m1.putIfAbsent(k, 0);
					m2.putIfAbsent(k, 0);
					
					finalResult.append(k+v).append("=").append(m1.get(k)).append("+").append(+m2.get(k)).append("\n");
					
					
					
				});
				
			    return finalResult.toString();
			});

			String finalResult=combinedMap.get();
			


			logger.info(finalResult);
			
		}
		
	}
	
}
