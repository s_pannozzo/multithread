package com.assignment.multithread;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
@Profile("default")

public class MultithreadApplication{

	public static void main(String[] args) {
		SpringApplication.run(MultithreadApplication.class, args);
	}

	
}
